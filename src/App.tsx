import {
  Deck,
  Slide,
  Heading,
  DefaultTemplate,
  Text,
  SlideLayout,
  Notes,
  UnorderedList,
  ListItem,
  Stepper,
  FlexBox,
  Image,
  Box,
} from "spectacle";
import emtekparrot from "./assets/emtek-parrot.gif";
import thisisfine from "./assets/this-is-fine.gif";

function App() {
  return (
    <Deck template={<DefaultTemplate />}>
      <SlideLayout.Center>
        <Heading>Hi everyone :-)</Heading>
        <Text>
          Lets talk about Emtek's visualizer
          <img src={emtekparrot} style={{ marginBottom: "-1em" }} />
        </Text>
      </SlideLayout.Center>
      <Slide>
        <Heading>Wat</Heading>
        <iframe
          style={{ height: "65%" }}
          src="https://cdn.dev.emtekaws.com/visualizer/?referID=8267&rule-1=371259&rule-2=371252&rule-7=371323&rule-8=371254&rule-5=371434&rule-11=371434&rule-6=371254&rule-12=371254&mirror=false&currentStep=0"
        />
        <Notes>
          <p>Emtek sells door hardware and other stuff</p>
          <p>explain basic visualizer flow</p>
          <p>So far, so ho hum</p>
        </Notes>
      </Slide>
      <Slide>
        <Heading>How many doorknobs could there possible be?!?</Heading>
        <UnorderedList margin={0.5}>
          <ListItem>35 rosettes (210 finish combinations)</ListItem>
          <ListItem>
            148 standard knobs/levers (1017 finish combinations)
          </ListItem>
          <ListItem>
            12 handles (101 finish combinations) and 4 stems (37 finish
            combinations)
          </ListItem>
          <ListItem>
            Total: 650k possible combinations
            <img src={thisisfine} style={{ marginBottom: "-1em" }} />
          </ListItem>
          <Notes>ain't no WAY we're doing that photoshoot</Notes>
        </UnorderedList>
      </Slide>
      <SlideLayout.Section>
        <Heading>So how does this work, anyway?</Heading>
        <Text>tldr: Blender's python api</Text>
      </SlideLayout.Section>
      <Slide>
        <Heading>Step one: assets</Heading>
        <Stepper
          alwaysVisible
          values={["Models", "Simple textures", "Fancy-ass textures", "Scene"]}
        >
          {(_value, step, isActive) => {
            if (!isActive) {
              return <Text>...So many assets</Text>;
            }
            return (
              <FlexBox flexWrap="wrap" alignItems="top">
                <Box maxWidth="30%">
                  <Text>Models</Text>
                  <Image src="broken.png" alt="an untextured model" />
                </Box>
                {step > 0 && (
                  <Box maxWidth="30%">
                    <Text>Simple textures</Text>
                    <Image
                      src="broken.png"
                      alt="a sphere with a brass texture next to the model from the previous step with the same brass texture"
                    />
                  </Box>
                )}
                {step > 1 && (
                  <Box maxWidth="40%">
                    <Text>Fancy-ass textures</Text>
                    <Image
                      src="broken.png"
                      alt="the same model as before, in oil rubbed bronze"
                    />
                  </Box>
                )}
                {step > 2 && (
                  <Box>
                    <Text>Scene and lighting</Text>
                    <Image
                      src="broken.png"
                      alt="spotlight shining in your face"
                    />
                  </Box>
                )}
              </FlexBox>
            );
          }}
        </Stepper>
      </Slide>
    </Deck>
  );
}

export default App;
